# README

This is a reboot of my previous budget app, with a better understanding of rails.

## To do list

- [ ] Improve css throughout
- [x] Redo budget page styles 
- [x] Add budget recap to calendar pages
- [x] Add budget totals to budget page
- [ ] Removed unused/empty files