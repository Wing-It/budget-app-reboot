Rails.application.routes.draw do
  root    "sessions#home"
  get     '/signup',      to: 'users#new'
  post    '/signup',      to: 'users#create'
  get     '/login',       to: 'sessions#new'
  post    '/login',       to: 'sessions#create'
  delete  '/logout',      to: 'sessions#destroy'
  get     '/budget',      to: 'budgets#show'
  post    '/newbudget',   to: 'budgets#create'
  post    '/budget',      to: 'budgets#edit'
  delete  '/budget',      to: 'budgets#destroy'
  get     '/init',        to: 'budgets#init'
  get     '/calendar',    to: 'months#show'
  post    '/bank',        to: 'months#edit'
  patch   '/reset/:id',   to: 'months#reset', as: 'reset'
  post    '/change/:dir', to: 'months#changemonth', as: 'change'
  post    '/newitem',     to: 'items#create'
  post    '/edititem',    to: 'items#edit'
  post    '/import',      to: 'transactions#create'
  patch   '/import',      to: 'transactions#newlist'
  get     '/import',      to: 'transactions#import'
  patch   '/transaction', to: 'transactions#update'
  
  get     '/canvas',      to: 'canvas#show'
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :months,              only: [:show, :destroy]
end
