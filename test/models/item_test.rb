require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @month = @user.months.create(firstday: Date.current)
    @day = @month.days.create(day: 0)
    @item = Item.new(category: 'budget', title: 'Sample Budget', amount: 125.50, day_id: @day.id)
  end
  
  test "should be valid" do
    assert @item.valid?, "#{@item.errors.full_messages.first}"
    @item.subtype = 'automatic'
    assert @item.valid?
    @item.actual = 'test actual'
    assert @item.valid?
  end
 
  test "must have day_id" do
    @item.day_id = nil
    assert_not @item.valid?
  end
  
  test "must have title" do
    @item.title = nil
    assert_not @item.valid?
  end
  
  test "must have valid category" do
    @item.category = nil
    assert_not @item.valid?
    @item.category = "test"
    assert_not @item.valid?
  end
  
  test "only valid subtypes allowed" do
    @item.subtype = "test"
    assert_not @item.valid?
    SUBTYPES.each do |subt|
      @item.subtype = subt
      assert @item.valid?
    end
  end
  
  test "must have amount" do
    @item.amount = nil
    assert_not @item.valid?
  end
  
  test "amount must be positive value" do
    @item.amount = -1.0
    assert_not @item.valid?
  end
  
  test "all categories valid" do
    CATEGORIES.each do |cat|
      @item.category = cat
      assert @item.valid?
    end
  end

end
