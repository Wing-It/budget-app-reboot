require 'test_helper'

class DayTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @month = @user.months.create(firstday: Date.current)
    @day = Day.new(day: 0, month_id: @month.id)
  end
  
  test "should be valid" do
    assert @day.valid?
  end

  test "must have user_id" do
    @day.month_id = nil
    assert_not @day.valid?
  end
  
  test "must have date" do
    @day.day = nil
    assert_not @day.valid?
  end
  
  test "only valid dates allowed" do
    @day.day = -1
    assert_not @day.valid?
    @day.day = 32
    assert_not @day.valid?
    @day.day = 0
    assert @day.valid?
    @day.day = 31
    assert @day.valid?
  end

end
