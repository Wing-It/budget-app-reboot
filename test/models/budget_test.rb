require 'test_helper'

class BudgetTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @budget = Budget.new(category: 'budget', title: 'Sample Budget', amount: 125.50, date: 0, user_id: @user.id)
  end
  
  test "should be valid" do
    assert @budget.valid?
    @budget.subtype = 'automatic'
    assert @budget.valid?
  end
  
  test "must have user_id" do
    @budget.user_id = nil
    assert_not @budget.valid?
  end
  
  test "must have title" do
    @budget.title = nil
    assert_not @budget.valid?
  end
  
  test "must have date" do
    @budget.date = nil
    assert_not @budget.valid?
  end
  
  test "must have valid category" do
    @budget.category = nil
    assert_not @budget.valid?
    @budget.category = "test"
    assert_not @budget.valid?
  end
  
  test "only valid subtypes allowed" do
    @budget.subtype = "test"
    assert_not @budget.valid?
    SUBTYPES.each do |subt|
      @budget.subtype = subt
      assert @budget.valid?
    end
  end
  

  test "only valid dates allowed" do
    @budget.date = -1
    assert_not @budget.valid?
    @budget.date = 46
    assert_not @budget.valid?
    @budget.date = 0
    assert @budget.valid?
    @budget.date = 45
    assert @budget.valid?
  end
  
  test "must have amount" do
    @budget.amount = nil
    assert_not @budget.valid?
  end
  
  test "amount must be positive value" do
    @budget.amount = -1.0
    assert_not @budget.valid?
  end
  
  test "all categories valid" do
    CATEGORIES.each do |cat|
      @budget.category = cat
      assert @budget.valid?
    end
  end
  
end
