require 'test_helper'

class MonthTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:michael)
    @month = Month.new(firstday: Date.current, user_id: @user.id)
  end
  
  test "should be valid" do
    assert @month.valid?
  end
  
  test "must have user_id" do
    @month.user_id = nil
    assert_not @month.valid?
  end
  
  test "must have firstday" do
    @month.firstday = nil
    assert_not @month.valid?
  end
  
  test "should set day to 1" do
    @month.firstday = Date.new(2016, 11, 20)
    @month.save
    assert_equal @month.firstday.day, 1
  end
  
  test "startbank should default to 500" do
    @month.save
    assert_equal @month.startbank, 500
  end
  
  test "should accept other values for startbank" do
    @month.startbank = 125
    @month.save
    assert_equal @month.startbank, 125
    @month.startbank = 250
    assert @month.save
  end
  
  test "should not allow same month per user" do
    assert @month.valid?
    @month.save
    #@month = Month.new(firstday: Date.current, user_id: @user.id)
    month = @user.months.new(firstday: Date.current)
    assert_not month.valid?
  end
end
