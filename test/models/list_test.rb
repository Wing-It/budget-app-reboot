require 'test_helper'

class ListTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @list = @user.lists.new(title: 'Test Item', category: 'check')
  end
  
  test "should be valid" do
    assert @list.valid?
  end
  
  test "duplicates not valid" do
    @list.save
    newlist = @user.lists.new(title: 'Test Item', category: 'check')
    assert_not newlist.valid?
  end
  
  test "assigns bankCategory" do
    @list.save
    assert_equal @list.bankCategory, 'credit'
    newlist = @user.lists.new(title: 'Test Item', category: 'budget')
    assert newlist.valid?
    newlist.save
    assert_equal newlist.bankCategory, 'debit'
  end
end
