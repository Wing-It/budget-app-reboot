require 'test_helper'

class MonthsViewTestTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
    @other = users(:archer)
    @month = @user.months.create(firstday: Date.current)
    Day.populate(@month)
    @othermonth = @other.months.create(firstday: Date.current)
    Day.populate(@month)
  end
  
  test "should not access month if not logged in" do
    get calendar_path
    assert_redirected_to login_url
  end
  
  test "should not access other users months" do
    get login_path
    post login_path, params: { session: { email: @user.email, password: 'password' } }
    @request.session[:month_id] = @othermonth.id
    assert_equal @request.session[:month_id], @othermonth.id
    get calendar_path
    assert_equal @request.session[:month_id], @month.id
    assert_template 'months/show'
  end
  
  test "should show month if logged in and belongs to user" do
    get login_path
    post login_path, params: { session: { email: @user.email, password: 'password' } }
    get calendar_path
    assert_template 'months/show'
  end
end
