class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.belongs_to :day, index: true
      t.string :title
      t.string :category, index: true
      t.string :subtype, index: true
      t.decimal :amount, precision: 7, scale: 2
      t.string :actual

      t.timestamps
    end
  end
end
