class CreateBudgets < ActiveRecord::Migration[5.0]
  def change
    create_table :budgets do |t|
      t.belongs_to :user, index: true
      t.string :title
      t.string :subtype, index: true
      t.decimal :amount, precision: 7, scale: 2
      t.string :category, index: true
      t.integer :date, index: true

      t.timestamps
    end
  end
end
