class CreateHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :histories do |t|
      t.belongs_to :user, index: true
      t.string :actual
      t.string :title
      t.string :category

      t.timestamps
    end
    add_index :histories, [:user_id, :title, :category] 
  end
end
