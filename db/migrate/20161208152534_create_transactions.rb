class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.belongs_to :month, index: true
      t.integer :day
      t.string :category
      t.string :title
      t.decimal :amount, precision: 7, scale: 2
      t.decimal :assigned, precision: 7, scale: 2, default: 0
      t.boolean :entered

      t.timestamps
    end
  end
end
