class CreateLists < ActiveRecord::Migration[5.0]
  def change
    create_table :lists do |t|
      t.belongs_to :user, index: true
      t.string :title, index: true
      t.string :category, index: true
      t.string :bankCategory, index: true

      t.timestamps
    end
  end
end
