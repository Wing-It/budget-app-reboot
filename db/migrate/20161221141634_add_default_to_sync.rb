class AddDefaultToSync < ActiveRecord::Migration[5.0]
  def change
    change_column :months, :sync, :integer, :default => 0
  end
end
