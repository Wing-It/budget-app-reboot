class CreateMonths < ActiveRecord::Migration[5.0]
  def change
    create_table :months do |t|
      t.belongs_to :user, index: true
      t.date :firstday
      t.decimal :startbank, precision: 7, scale: 2, default: 500
      t.decimal :endbank, precision: 7, scale: 2
      t.integer :sync

      t.timestamps
    end
    
    add_index :months, :firstday
  end
end
