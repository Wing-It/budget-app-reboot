# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161221141634) do

  create_table "budgets", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "subtype"
    t.decimal  "amount",     precision: 7, scale: 2
    t.string   "category"
    t.integer  "date"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["category"], name: "index_budgets_on_category"
    t.index ["date"], name: "index_budgets_on_date"
    t.index ["subtype"], name: "index_budgets_on_subtype"
    t.index ["user_id"], name: "index_budgets_on_user_id"
  end

  create_table "days", force: :cascade do |t|
    t.integer  "month_id"
    t.integer  "day"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["day"], name: "index_days_on_day"
    t.index ["month_id"], name: "index_days_on_month_id"
  end

  create_table "histories", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "actual"
    t.string   "title"
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id", "title", "category"], name: "index_histories_on_user_id_and_title_and_category"
    t.index ["user_id"], name: "index_histories_on_user_id"
  end

  create_table "items", force: :cascade do |t|
    t.integer  "day_id"
    t.string   "title"
    t.string   "category"
    t.string   "subtype"
    t.decimal  "amount",     precision: 7, scale: 2
    t.string   "actual"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["category"], name: "index_items_on_category"
    t.index ["day_id"], name: "index_items_on_day_id"
    t.index ["subtype"], name: "index_items_on_subtype"
  end

  create_table "lists", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "category"
    t.string   "bankCategory"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["bankCategory"], name: "index_lists_on_bankCategory"
    t.index ["category"], name: "index_lists_on_category"
    t.index ["title"], name: "index_lists_on_title"
    t.index ["user_id"], name: "index_lists_on_user_id"
  end

  create_table "months", force: :cascade do |t|
    t.integer  "user_id"
    t.date     "firstday"
    t.decimal  "startbank",  precision: 7, scale: 2, default: "500.0"
    t.decimal  "endbank",    precision: 7, scale: 2
    t.integer  "sync",                               default: 0
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.index ["firstday"], name: "index_months_on_firstday"
    t.index ["user_id"], name: "index_months_on_user_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.integer  "month_id"
    t.integer  "day"
    t.string   "category"
    t.string   "title"
    t.decimal  "amount",     precision: 7, scale: 2
    t.decimal  "assigned",   precision: 7, scale: 2, default: "0.0"
    t.boolean  "entered"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.index ["month_id"], name: "index_transactions_on_month_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "password_digest"
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.boolean  "init",              default: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
