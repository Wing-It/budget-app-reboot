class Budget < ApplicationRecord
  belongs_to :user, touch: true
  validates :user, presence: true
  validates :category, inclusion: { in: CATEGORIES, message: "%{value} is not a valid category" }
  validates :subtype, inclusion: { in: SUBTYPES, message: "%{value} is not a valid subtype" }, allow_nil: true
  validates :title, presence: true
  validates :amount, numericality: { greater_than: 0 }
  validates :date, inclusion: { in: 0..45, message: "invalid value" }
  
  private

end
