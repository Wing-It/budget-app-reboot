class Day < ApplicationRecord
  belongs_to :month
  has_many :items, dependent: :destroy
  validates :month, presence: true
  validates :day, inclusion: { in: 0..31, message: "invalid value" }
  
  def Day.populate(month)
    user = User.find(month.user_id)
    @budgets = user.budgets.all
    @day = month.days.create(day: 0)
    @budgets.where(subtype: 'unscheduled').each do |x|
      Item.frombudget(@day, x)
    end
    month.loopDays do |d|
      @day = month.days.create(day: d.day)
      Day.daybudgets(d)
    end
  end
  
  def Day.daybudgets(date)
    if (date.wday == 1) && (date.day != 1)
      minus = date.day == 2 ? 1 : 2
      monthly = [ date.day + (13 - minus)..@day.day + 13]
    else
      monthly = @day.day + 13
    end
    
    if date.weekday?
      @budgets.where( date: monthly ).each do |x|
        Item.frombudget(@day, x)
      end
    end
    
    @budgets.where( date: date.wday ).each do |x|
      Item.frombudget(@day, x)
    end
    
    if date.strftime("%U").to_i % 2 == 0
      @budgets.where( date: date.wday + 7 ).each do |x|
        Item.frombudget(@day, x)
      end
    end
  end
  
  def categorySymbol( cat )
    symbol = 'money'
    calendar = false
    sync = true
    items.where(category: cat).each do |x|
      symbol = 'check' if x.itemIcon == 'check'
      calendar = true if x.itemIcon == 'calendar'
      sync = false if x.itemIcon == 'times'
    end
    symbol = 'calendar' if calendar
    symbol = 'times' unless sync
    return symbol
  end
  
  def isWrong( recurse = true )
    wrong = false
    month = Month.find( month_id )
    if month.sync > 0
      items.each { |x| wrong = true if x.itemIcon == 'times' }
      if day > 1
        wrong = true if month.days.find_by( day: day-1 ).isWrong( false )
      end
    end
    return wrong
  end
  
  private
    
end
