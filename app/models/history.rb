class History < ApplicationRecord
  belongs_to :user
  validates :user, presence: true
  validates :actual, uniqueness: { scope: :category }, presence: true
  validates :title, presence: true
  validates :category, inclusion: { in: ["credit", "debit"], message: "%{value} is not a valid category" }
  
end
