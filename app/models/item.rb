class Item < ApplicationRecord
  belongs_to :day
  validates :day, presence: true
  validates :category, inclusion: { in: CATEGORIES, message: "%{value} is not a valid category" }
  validates :subtype, inclusion: { in: SUBTYPES, message: "%{value} is not a valid subtype" }, allow_nil: true
  validates :title, presence: true
  validates :amount, numericality: { greater_than: 0 }
  
  def Item.frombudget(d, b)
    d.items.create(category: b.category, subtype: b.subtype, title: b.title, amount: b.amount)
  end
  
  def itemIcon
    day = Day.find( day_id )
    month = Month.find( day.month_id )
    sync = day.day <= month.sync
    icon = actual ? 'check' : 'calendar'
    icon = 'times' if ( !actual && sync )
    icon = 'money' if subtype == 'cash'
    return icon
  end
end
