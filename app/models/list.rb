class List < ApplicationRecord
  belongs_to :user
  validates :user, presence: true
  validates :title, uniqueness: { scope: [:category, :user] }, presence: true
  validates :category, inclusion: { in: CATEGORIES, message: "%{value} is not a valid category" }
  validates :bankCategory, inclusion: { in: ["credit", "debit"], message: "%{value} is not a valid category" }
  before_validation :set_bank_category
  
  private
    def set_bank_category
      if self.category
        self.bankCategory = self.category == 'check' ? 'credit' : 'debit'
      end
    end
end
