class User < ApplicationRecord
  has_many :months, dependent: :destroy
  has_many :budgets, dependent: :destroy
  has_many :lists, dependent: :destroy
  has_many :histories, dependent: :destroy
  has_many :days, through: :months, dependent: :destroy
  has_many :items, through: :days, dependent: :destroy
  has_many :transactions, through: :month, dependent: :destroy
  attr_accessor :activation_token, :reset_token
  before_save :downcase_email
  before_create :create_activation_digest
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  def authenticated?(attribute, token)
    digest = self.send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  
  def activate
    update_columns(activated: true, activated_at: Time.zone.now)
    create_budgets
    #create_lists
  end
  
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  def create_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest: User.digest(reset_token), reset_sent_at: Time.zone.now)
  end
  
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
  
  private
    
    def downcase_email
      self.email = email.downcase
    end
    
    def create_activation_digest
      self.activation_token = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
    
    def create_budgets
      budget_seeds = [{:category => 'check', :title => 'Paycheck', :amount => 400, :date => 5},
                      {:category => 'check', :title => 'Paycheck Two', :amount => 400, :date => biweekly(5)},
                      {:category => 'bill', :title => 'Electric', :amount => 190, :date => monthly(5)},
                      {:category => 'bill', :title => 'Internet', :amount => 60, :date => monthly(5)},
                      {:category => 'bill', :title => 'Rent', :amount => 550, :date => monthly(6)},
                      {:category => 'bill', :title => 'Water', :subtype => 'automatic', :amount => 40, :date => monthly(12)},
                      {:category => 'bill', :title => 'Netflix', :subtype => 'automatic', :amount => 11, :date => monthly(25)},
                      {:category => 'budget', :title => 'Groceries', :amount => 60, :date => 2},
                      {:category => 'budget', :title => 'Savings', :amount => 25, :date => 1},
                      {:category => 'budget', :title => 'Gas', :subtype => 'unscheduled', :amount => 100, :date => 45},
                      {:category => 'budget', :title => 'Restaurants', :subtype => 'unscheduled', :amount => 100, :date => 45},
                      {:category => 'budget', :title => 'Spending', :subtype => 'cash', :amount => 30, :date => 5}]
      budget_seeds.each do |seed|
        self.budgets.create seed unless self.budgets.exists?({title: seed[:title]})
      end
    end
    
    def create_lists
      list_seeds = [{:category => 'check', :title => 'Paycheck'},
                    {:category => 'check', :title => 'Paycheck Two'},
                    {:category => 'bill', :title => 'Electric'},
                    {:category => 'bill', :title => 'Internet'},
                    {:category => 'bill', :title => 'Rent'},
                    {:category => 'bill', :title => 'Water'},
                    {:category => 'bill', :title => 'Netflix'},
                    {:category => 'budget', :title => 'Groceries'},
                    {:category => 'budget', :title => 'Savings'},
                    {:category => 'budget', :title => 'Gas'},
                    {:category => 'budget', :title => 'Restaurants'},
                    {:category => 'budget', :title => 'Spending'}]
      list_seeds.each do |seed|
        self.lists.create seed
      end
    end
end
