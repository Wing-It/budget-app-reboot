class Month < ApplicationRecord
  belongs_to :user, touch: true
  has_many :days, dependent: :destroy
  has_many :items, through: :days, dependent: :destroy
  has_many :transactions
  validates :user, presence: true
  validates :firstday, presence: true
  validates :startbank, numericality: true
  validates :endbank, numericality: true, allow_nil: true
  validate :unique_dates_per_user, on: :create
  before_save :set_day
  
  def Month.findMonth(user, date = Date.current)
    date = Date.new(date.year, date.month, 1)
    if user.months.exists?({firstday: date})
      @month = user.months.find_by(firstday: date)
    else
      @month = user.months.create(firstday: date)
      @month.previousBank
      Day.populate(@month)
    end
    return @month
  end
  
  def loopDays
    looper = firstday
    while looper.month == firstday.month do
      yield(looper)
      looper = looper.tomorrow
    end
    looper = looper.yesterday
    return( looper.wday )
  end
  
  def previous
    @user = User.find(user_id)
    @user.months.find_by({ firstday: firstday << 1 })
  end
  
  def previousBank
    if previous
      self.update_attribute(:startbank, previous.endbank) if previous.endbank
    end
  end
  
  def distinctItems( cat )
    itemlist = []
    days.each do |d|
      d.items.where(category: cat).each do |x|
        itemlist.push( x.title ) unless itemlist.include?( x.title )
      end
    end
    return itemlist.sort
  end
  
  def status( title )
    symbol = 'check'
    days.each do |d|
      if d.day != 0
        d.items.where( title: title ).each do |x|
          symbol = 'calendar' if ( ( x.subtype != 'cash' ) && !x.actual ) || ( ( x.subtype == 'cash' ) && ( d.day > sync ) )
        end
      end 
    end 
    return symbol
  end
  
  private
    def set_day
      self.firstday = first_day(firstday)
    end
    
    def first_day(date)
      Date.new(date.year, date.month, 1)
    end
    
    def unique_dates_per_user
      if firstday
        monthValue = first_day(firstday)
        if Month.exists?({ firstday: monthValue, user_id: user_id })
          errors.add(:firstday, "cannot have multiple entries per month" )
        end
      end
    end
    
end
