class CanvasController < ApplicationController
  
  def show
    @teeth = []
    quadrant = [ 'Upper Right', 'Upper Left', 'Lower Left', 'Lower Right' ]
    for x in 0..3
      range = Array( 1..8 )
      range.reverse! if x % 2 == 0
      for y in range
        @teeth.push( quadrant[x] + ' ' + y.to_s )
      end
    end
  end
  
end
