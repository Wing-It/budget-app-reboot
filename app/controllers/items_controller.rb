class ItemsController < ApplicationController
  def create
    @month = Month.find(params[:month_id])
    @user = current_user
    item = @month.items.new(item_params)
    if item.save
      @user.lists.create({title: item.title, category: item.category})
      flash[:success] = "Item added successfully"
      redirect_to calendar_url
    else
      flash[:danger] = item.errors.full_messages.first
      redirect_to calendar_url
    end
  end
  
  def edit
    @user = current_user
    item = Item.find params[:item][:id]
    if params[:delete]
      if item.destroy
        flash[:success] = "Item deleted successfully"
        redirect_to calendar_url
      end
    else
      if item.update_attributes(item_params)
        @user.lists.create({title: item.title, category: item.category})
        flash[:success] = "#{item.title} successfully updated"
        redirect_to calendar_url
      else
        flash[:danger] = item.errors.full_messages.first
        redirect_to calendar_url
      end
    end
  end
  
  private
    def item_params
      params.require(:item).permit(:category, :subtype, :title, :amount, :day_id)
    end
end
