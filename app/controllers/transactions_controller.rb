class TransactionsController < ApplicationController
  require 'csv'
  
  def create
    @user = current_user
    uploaded = params[:transactions][:csvfile]
    if uploaded
      csvText = uploaded.read
      quote_chars = ["\"","|","~","^","&","*"]
      begin
        csv = CSV.parse(csvText, headers: :first_row, quote_char: quote_chars.shift)
      rescue CSV::MalformedCSVError
        quote_chars.empty? ? raise : retry
      end
      tempArray = []
      csv.each do |row|
        row.each do |r|
          r[1] = r[1].gsub(/["]/, '')
        end
        amount = row[2] == "DEBIT" ? row[4].gsub('-', '') : row[5].gsub('-', '')
        date = Date.strptime(row[0], '%m/%d/%Y')
        month = Month.findMonth(@user, date)
        if !month.transactions.exists?({day: date.day, category: row[2].downcase, title: row[3], amount: amount})
          tempArray.push([month.id,{day: date.day, category: row[2].downcase, title: row[3], amount: amount, entered: false}])
        end
      end
      tempArray.each { |x| Month.find(x[0]).transactions.create(x[1]) }
    end
    @month = Month.find(params[:transactions][:month_id])
    redirect_to import_path
  end
  
  def update
    @user = current_user
    @month = Month.find(session[:month_id])
    @list = List.find(params[:transaction][:title])
    @transaction = Transaction.find(params[:transaction][:id])
    @day = @month.days.find_by({ day: @transaction.day })
    if params[:planned]
      @item = Item.find(params[:planned])
      success = true if @item.update_attributes({ actual: @transaction.title, amount: @transaction.amount, day_id: @day.id })
    else
      success = true if @day.items.create({ title: @list.title, category: @list.category, amount: @transaction.amount, actual: @transaction.title })
    end
    if success
      @transaction.update_attribute :entered, true
      @month.update_attribute :sync, @transaction.day
      history = @user.histories.find_by({ actual: @transaction.title, category: @transaction.category })
      if history
        history.update_attribute( :title, @list.title )
      else
        @user.histories.create({ actual: @transaction.title, category: @transaction.category, title: @list.title })
      end
    end
    redirect_to import_path
  end
  
  def import
    @user = current_user
    @month = Month.find(session[:month_id])
    @transaction = @month.transactions.where({entered: false}).first
    if @transaction
      if !@firstitem
        history = @user.histories.find_by({ actual: @transaction.title, category: @transaction.category })
        if history
          list = @user.lists.find_by({ title: history.title, bankCategory: history.category })
          if list
            @firstitem = list.id
          end
        end
      end
      @list = @user.lists.where( bankCategory: @transaction.category ).collect { |l| [ l.title, l.id ] }
      @list.push [ 'New Item', '' ]
      render 'import'
    else
      redirect_to root_path
    end
  end
  
  def newlist
    @user = current_user
    list = @user.lists.new(list_params)
    if list.save
      @firstitem = list.id
      import
    else
      flash[:danger] = list.errors.full_messages.first
      redirect_to import_url
    end
  end
  
  private
    def list_params
      params.require(:lists).permit(:title, :category)
    end
end

