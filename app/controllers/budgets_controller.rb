class BudgetsController < ApplicationController
  before_action :logged_in_user, only: [:show]
  
  def show
    @user = current_user
    if @user.init
      @month = Month.findMonth(@user)
    else
      date = Date.current
      @month = @user.months.new( firstday: Date.new(date.year, date.month, 1) )
    end
    @budgets = @user.budgets.all
    @list = { :check    => [ nil ],
              :bill     => [ nil, "automatic" ],
              :budget   => [ nil, "cash", "unscheduled" ] }
    @budget = @user.budgets.build
  end
  
  def create
    #render plain: params.to_yaml
    @user = current_user
    budget = @user.budgets.new(budget_params)
    if budget.save
      @user.lists.create({title: budget.title, category: budget.category}) if @user.init
      flash[:success] = "Item added successfully"
      redirect_to budget_url
    else
      flash[:danger] = budget.errors.full_messages.first
      redirect_to budget_url
    end
  end
  
  def edit
    #render plain: params.to_yaml
    @user = current_user
    budget = Budget.find params[:budget][:id]
    if params[:delete]
      if budget.destroy
        flash[:success] = "Item deleted successfully"
        @user.lists.find_by({title: budget.title, category: budget.category}).destroy if @user.init
        redirect_to budget_url
      end
    else
      if budget.update_attributes(budget_params)
        @user.lists.create({title: budget.title, category: budget.category}) if @user.init
        flash[:success] = "#{budget.title} successfully updated"
        redirect_to budget_url
      else
        flash[:danger] = budget.errors.full_messages.first
        redirect_to budget_url
      end
    end
  end
  
  def init
    @user = current_user
    if @user.update_attribute( :init, true )
      @user.budgets.each do |b|
        @user.lists.create({title: b.title, category: b.category})
      end
      session[:month_id] = Month.findMonth(@user).id
    end
    redirect_to calendar_url
  end
  
  private
    def budget_params
      params.require(:budget).permit(:category, :subtype, :title, :amount, :date)
    end
end
