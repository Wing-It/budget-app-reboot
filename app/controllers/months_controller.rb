class MonthsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user
  
  def show
    @item = @month.items.build
    @bank = @month.startbank
    @budget = @user.budgets.all
    @sync = @month.sync ? @month.sync : 0
  end
  
  def destroy
    oldmonth = Month.find(params[:id])
    date = oldmonth.firstday
    @user = current_user
    oldmonth.destroy
    newmonth = Month.findMonth(@user, date)
    oldmonth.transactions.each { |x| x.update_attributes({ month_id: newmonth.id, assigned: 0, entered: false }) }
    session[:month_id] = newmonth.id
    redirect_to calendar_url
  end
  
  def reset
    @month.previousBank
    redirect_to calendar_url
  end
  
  def edit
    if @month.update_attributes(month_params)
      flash[:success] = "Bank balance successfully updated"
      redirect_to calendar_url
    else
      flash[:danger] = @month.errors.full_messages.first
      redirect_to calendar_url
    end
  end
  
  def changemonth
    newmonth = params[:dir] == 'right' ? @month.firstday >> 1 : @month.firstday << 1
    @month = Month.findMonth(@user, newmonth)
    session[:month_id] = @month.id
    redirect_to calendar_url
  end
  
  private
    def month_params
      params.require(:month).permit(:startbank)
    end
    
    def correct_user
      @user = current_user
      if @user.init
        @month = current_month
        if(@month.user_id != @user.id)
          flash[:danger] = "Action not authorised"
          session[:month_id] = Month.findMonth(user).id
          redirect_to :back
        end
      else
        flash[:danger] = "Review/change budget and confirm before viewing calendar"
        redirect_to budget_path
      end
    end
  
end
