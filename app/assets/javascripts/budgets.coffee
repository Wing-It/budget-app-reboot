@budgetform = (form, category, subcat, name, amount, id, date, freq) ->
  $( '#' + form + 'budget_budget_subtype' ).attr( 'name', '' )
  $( '#' + form + 'budget_budget_category' ).val( category )
  $( '#' + form + 'datehide' ).show()
  if subcat? && subcat != ''
    $( '#' + form + 'budget_budget_subtype' ).attr( 'name', 'budget[subtype]' )
    $( '#' + form + 'budget_budget_subtype' ).val( subcat )
    if subcat == 'unscheduled'
      $( '#' + form + 'datehide' ).hide()
      $( '#' + form + 'budget input[type="radio"]' ).prop( "checked", false )
      $( '#' + form + 'budget_budget_date_45' ).prop( "checked", true )
  if name?
    $( '#' + form + 'budget_budget_title' ).val( name )
    $( '#' + form + 'budgetLabel' ).html( 'Edit ' + name )
  if amount?
    $( '#' + form + 'budget_budget_amount' ).val( amount )
  if id?
    $( '#' + form + 'budget_budget_id' ).val( id )
  if date?
    $( '#' + form + 'budget input[type="radio"]' ).prop( "checked", false )
    $( '#' + form + 'budget_budget_date_' + date ).prop( "checked", true )
    $( '#' + form + 'datehide li, #' + form + 'datehide div' ).removeClass( 'active' )
    $( '#' + form + 'budget' + freq + ', #' + form + freq + 'tab' ).addClass( 'active' )
    #alert( '#' + form + 'budget' + freq )