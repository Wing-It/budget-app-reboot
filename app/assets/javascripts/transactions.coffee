@selectTitle = () ->
  $( 'option:empty' ).remove()
  $( '#calendar' ).removeClass( 'inactive' )
  $( '#plannedCheckbox' ).css('display', 'none')
  $( '.dateTitle, .dateCell' ).removeClass( 'selected active activeBox linkMouse' )
  $( '#planned' ).prop( "checked", false )
  $( '.planned' ).removeClass( 'shown' )
  $( '.planned' ).hide()
  listvalue = $( '#transaction_title' ).val()
  if listvalue is ''
    $( '#import' ).modal('show')
  else
    dayItems = $( 'div[data-list-id=' + listvalue + ']' )
    if dayItems.length > 0
      $( '#calendar' ).addClass( 'inactive' )
      $( '#plannedCheckbox' ).css('display', 'inline-block')
      dayItems.each ( x ) ->
        $( this ).show()
        day = $( this ).data( 'day' )
        $( this ).addClass( 'shown' )
        $( '#' + day + 'title' ).addClass( 'active' )
        $( '#' + day + 'box' ).addClass( 'activeBox' )
        selectIt( day ) if x == 0
        return
      return
      
@selectIt = ( day ) ->
  $( '.dateTitle, .dateCell' ).removeClass( 'selected' )
  if $( '#planned' ).is( ":checked" )
    $( '.activeBox' ).addClass( 'linkMouse' )
    $( '#' + day + 'title, #' + day + 'box' ).addClass( 'selected' )
    item = $( '#' + day + 'box .shown' ).data( 'item-id' )
    $( '#planned' ).val( item )

@checkPlanned = ( element ) ->
  day = $( '.shown' ).data( 'day' )
  selectIt( day )
  
@selectDay = ( day ) ->
  if $( '#' + day + 'title' ).hasClass( 'active' )
    selectIt( day )
    return
    
@switchItem = ( item ) ->
  $( '#transaction_title' ).val( item )
  selectTitle()