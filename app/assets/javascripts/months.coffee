@dayform = (form, selectday, category, name, amount, id) ->
  $( '#' + form + 'item_item_category' ).val( category )
  $( '[list]' ).attr( 'list', category )
  $( '#' + form + 'item input[type="radio"]' ).prop( "checked", false )
  $( '#' + form + 'item_item_day_id_' + selectday ).prop( "checked", true )
  if form is 'new'
    if category is 'budget'
      newTitle = 'Add new budget item'
    else
      newTitle = 'Add new ' + category
    $( '#' + form + 'itemLabel' ).html( newTitle )
  if name?
    $( '#' + form + 'item_item_title' ).val( name )
    $( '#' + form + 'itemLabel' ).html( 'Edit ' + name )
  if amount?
    $( '#' + form + 'item_item_amount' ).val( amount )
  if id?
    $( '#' + form + 'item_item_id' ).val( id )