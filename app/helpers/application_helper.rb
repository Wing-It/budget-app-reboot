module ApplicationHelper
  
  def fullTitle(pageTitle = '')
    baseTitle = "Budget App"
    if pageTitle.empty?
      baseTitle
    else
      pageTitle + ' | ' + baseTitle
    end
  end
end
