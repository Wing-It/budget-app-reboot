module BudgetsHelper
  def show_frequency(x)
    if x
      "#{freq_type(x).capitalize} on #{day_string(x)}"
    end
  end
  
  def day_string(x)
    if x
      if x.between?(0, 6)
        return Date::DAYNAMES[x]
      elsif x.between?(7, 13)
        return Date::DAYNAMES[x - 7]
      else
        return "the #{(x - 13).ordinalize}"
      end
    end
  end
  
  def date_number(x)
    if x
      if x.between?(0, 6)
        return x
      elsif x.between?(7, 13)
        return x - 7
      else
        return x - 13
      end
    end
  end
  
  def freq_type( x, num = false )
    if x
      if x.between?(0, 6)
        return num ? 4 : 'weekly'
      elsif x.between?(7, 13)
        return num ? 2 : 'biweekly'
      else
        return num ? 1 : 'monthly'
      end
    end
  end
  
  def monthly(x)
    x + 13
  end
  
  def biweekly(x)
    x + 7
  end
  
end
