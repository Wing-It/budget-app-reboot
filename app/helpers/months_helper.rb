module MonthsHelper
  
  def unscheduledSpent( title )
    dayzero = @month.days.find_by( day: 0 )
    @month.items.where(title: title).sum( :amount ) - dayzero.items.where(title: title).sum( :amount )
  end
  
  def spent( title, cat )
    total = 0
    @month.days.each do |d|
      total += d.items.where( title: title, category: cat ).sum( :amount ) if d.day != 0
    end
    return total
  end
  
  def budgeted( title, cat )
    budget = @user.budgets.find_by( title: title, category: cat )
    if budget
      return budget.amount * freq_type( budget.date, true )
    else
      return 0.to_d
    end
  end
  
  def budgetedTotal( cat )
    budget = 0
    @user.budgets.where( category: cat ).each { |x| budget += budgeted( x.title, x.category ) }
    return budget
  end
  
  def status( dif, cat )
    stat = dif >= 0 ? 'positive' : 'negative'
    stat = stat == 'positive' ? 'negative' : 'positive' if cat == 'check'
    stat = '' if dif == 0
    return stat
  end
end
