class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@monthlybudget.com'
  layout 'mailer'
end
