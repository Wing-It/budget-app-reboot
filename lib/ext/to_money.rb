class BigDecimal
  def to_money( minus = '-', base = false, symbol = "$" )
    sign = self < 0 ? minus : ''
    x = self < 0 ? self * -1 : self
    num = self % 1 == 0 && (base || self == 0) ? self.floor.to_s : ('%.2f' % x).to_s
    sign + symbol + num
  end
end
